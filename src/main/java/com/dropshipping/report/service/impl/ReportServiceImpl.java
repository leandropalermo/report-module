package com.dropshipping.report.service.impl;

import com.dropshipping.report.exception.NotFoundException;
import com.dropshipping.report.repository.ReportRepository;
import com.dropshipping.report.repository.document.ReportDocument;
import com.dropshipping.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportRepository reportRepository;

    ReportServiceImpl(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    @Override
    public ReportDocument getReportBySeller(final String seller) throws NotFoundException {
        Optional<ReportDocument> sellerFound = reportRepository.findBySeller(seller);
        return sellerFound.orElseThrow(() -> new NotFoundException("Seller not found."));
    }

    @Override
    public ReportDocument insertReport(ReportDocument reportDocument) {
        return reportRepository.insert(reportDocument);
    }

    @Override
    public void deleteReportBySeller(final ReportDocument reportDocument) {
        reportRepository.delete(reportDocument);
    }

    @Override
    public ReportDocument updateReport(final ReportDocument reportDocument) throws NotFoundException {
        getReportBySeller(reportDocument.getSeller());
        return reportRepository.save(reportDocument);
    }
}
