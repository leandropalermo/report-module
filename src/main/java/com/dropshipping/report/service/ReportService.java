package com.dropshipping.report.service;


import com.dropshipping.report.exception.NotFoundException;
import com.dropshipping.report.repository.document.ReportDocument;

public interface ReportService {

    ReportDocument getReportBySeller(final String seller) throws NotFoundException;
    ReportDocument insertReport(ReportDocument reportDocument) ;
    void deleteReportBySeller(final ReportDocument reportDocument);
    ReportDocument updateReport(final ReportDocument reportDocument) throws NotFoundException;
}
