package com.dropshipping.report.controller;

import com.dropshipping.report.exception.NotFoundException;
import com.dropshipping.report.repository.document.ReportDocument;
import com.dropshipping.report.service.ReportService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/report")
@RefreshScope
public class ReportController {

    private ReportService reportService;

    @Autowired
    ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/{seller}")
    public ReportDocument getReportBySeller(@PathVariable("seller") String seller) throws NotFoundException {
        ReportDocument reportDocument = reportService.getReportBySeller(seller);
        return reportDocument;
    }

    @PostMapping
    public ReportDocument insertReport(@RequestBody ReportDocument reportDocument) {
        return reportService.insertReport(reportDocument);
    }

    @DeleteMapping("/{seller}")
    public void deleteReportBySeller(@RequestBody ReportDocument reportDocument) {
        reportService.deleteReportBySeller(reportDocument);
    }

    @PutMapping
    public void updateReport(@RequestBody ReportDocument reportDocument) throws NotFoundException {
        reportService.updateReport(reportDocument);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NotFoundException.class})
    private String reportNotFound(NotFoundException e){
        String resp = e.getMessage();
        return resp;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    private String unexpectedError(Exception e) {
        String resp = "Unexpected error.";
        if (!StringUtils.isEmpty(e.getMessage())) {
            resp = e.getMessage();
        }
        return resp;
    }
}
