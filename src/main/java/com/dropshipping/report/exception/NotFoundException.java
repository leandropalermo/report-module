package com.dropshipping.report.exception;

public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}
