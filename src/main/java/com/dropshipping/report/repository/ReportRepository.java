package com.dropshipping.report.repository;

import com.dropshipping.report.repository.document.ReportDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReportRepository extends MongoRepository<ReportDocument, String> {

    Optional<ReportDocument> findBySeller(String seller);
}
