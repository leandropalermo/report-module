package com.dropshipping.report.repository.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "report")
@Data
public class ReportDocument {

    @Id
    private String id;
    private String seller;
    private int totalSold;

}
